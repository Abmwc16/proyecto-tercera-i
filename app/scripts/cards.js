const dataCards = [
    {
        "title": "Ventajas de Jugar en PC",
        "url_image":"http://greyhoundhope.org/wp-content/uploads/2020/12/Tech-4-En.jpg",
        "desc":"Ser parte de la comunidad PC master race y sorprendete con las ventajas de jugar en una PC gaming.",
        "cta":"Show More",
        "link":"/PC.html"
      },
      {
        "title": "PlayStation",
        "url_image":"https://i0.wp.com/kakuchopurei.com/wp-content/uploads/2020/11/PSconsolesSizeComparison_stack.png?resize=760%2C399&ssl=1",
        "desc":"Recomendaciones para disfrutar al maximo de tus consolas de PlayStation.",
        "cta":"Show More",
        "link":"/ps.html"
      },
      {
        "title": "Xbox",
        "url_image":"https://i.redd.it/va3pa949y7951.png",
        "desc":"Conoce mucho mas acerca de las caracteristicas de los nuevos Xbox de Microsoft.",
        "cta":"Show More",
        "link":"/xbox.html"
      },
      {
        "title": "Nintendo",
        "url_image":"https://dnlzsmybcpo0z.cloudfront.net/games/images/map_img_1924996_1602259122.jpg",
        "desc":"Diviertete solo o en familia con los jeugos exclusivos de Nintendo.",
        "cta":"Show More",
        "link":"/nintendo.html"
      }];

      (function() {
          let CARD = {
              init: function (){
              console.log('Card module was loaded');
              let _self = this;
              

              //llamamos las funciones
              this.insertData(_self);
              //this.eventHandler(_self);
            },

            eventHandler: function(_self) {
                let arrayRefs = document.querySelectorAll('.accordion-title');

                for (let x = 0; x < arrayRefs.length; x++) {
                    arrayRefs[x].addEventListener('click', function(event){
                        console.log('Event',event);
                        _self.showTab(event.target);
                    });
                }
            },

            insertData: function(_self) {
                dataCards.map(function (item, index) {
                    document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
                });
            },

            tplCardItem: function(item, index) {
              return (` <div class='card-item' id='card-number-${index}'>
              <img src="${item.url_image}"/>
              <div class= "card-info">
              <p class='card-title'>${item.title}</p>
              <p class='card-desc'>${item.desc}</p>
              <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
              </div>
              </div>`)},
          }//init
          CARD.init();
      })();